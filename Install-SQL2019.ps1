﻿#SQL Install Script#
#Date: 29/04/2021#
#Summary: This script is designed to install SQL Server 2019 replacing the DSC configuration script#

#Variables#
#Change These As Required#
$SQLVersion = "2019"
$DataDrive = "D"
$LogDrive = "L"
$BackUpDrive = "D"
$TempDBDrive = "T"
$Binaries = "B"
$Edition = "Dev"
$ComputerName = $env:COMPUTERNAME
###

#Register DFI Powershell Gallery
$Repos = Get-PSRepository | Select-Object Name
If ($Repos -match "DFIGallery") {
  Write-Host "DFI Gallery Has Registered."
 Continue
  }  Else {
 "DFI Gallery Has NOT Registered. Trying to register it."
 Register-PSRepository -Name DFIGallery -SourceLocation \\devbuild02\psnugetserver\ -InstallationPolicy Trusted
} 
###

#Check DFI Gallery Registered
$Repos = Get-PSRepository | Select-Object Name
If ($Repos -match "DFIGallery") {
  Write-Host "DFI Gallery Has Registered."
 Continue
  }  Else {
 "DFI Gallery Has NOT Registered. Go Fix It."
 #Exit
} 
###

# Install DBATools Module
Install-Module dbatools -Repository DFIGallery -Force
###

#Create Folders
$Path = "$DataDrive`:\Data"
If(!(test-path $path))
{
 New-Item -Name "Data" -Path "$DataDrive`:\" -ItemType Directory
}

$Path = "$LogDrive`:\Logs"
If(!(test-path $path))
{
 New-Item -Name "Logs" -Path "$LogDrive`:\" -ItemType Directory
}

$Path = "$TempDBDrive`:\TempDB"
If(!(test-path $path))
{
 New-Item -Name "TempDB" -Path "$TempDBDrive`:\" -ItemType Directory
}

$Path = "$BackUpDrive`:\Backup"
If(!(test-path $path))
{
 New-Item -Name "Backup" -Path "$BackUpDrive`:\" -ItemType Directory
}
###

# Installing Choco
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
###

# Installing ActiveDirectory Module
Install-WindowsFeature -Name RSAT-AD-PowerShell -IncludeAllSubFeature -ComputerName $ComputerName
###

# Copying SQL ISO To Server
$Path = "C:\SQL2019\"
If(!(test-path $path))
{
 New-Item -Name "SQL2019" -Path "C:\SQL2019\" -ItemType Directory
}
Copy-Item -Path "\\DBAPROD02\SQL_PS_Install\SQL2019\SQL2019\" -Destination "C:\SQL2019\" -Recurse -Force
###

# Install SQL Server
Import-Module ActiveDirectory -Force
Try {
    $filter = "Description -like ""$ComputerName*"""
    $svcname = Get-ADServiceAccount -Filter ("$filter")|where-object {$_.name -like "SQL*"} -ErrorAction Stop
    $svcid = $svcname.name.Substring($svcname.name.Length - 3)
}
catch {
    Write-output "MSA not Defined for this server"
    break
}
C:\SQL2019\setup.exe `
    /ACTION="Install" `
    /QUIETSIMPLE="True" `
    /UPDATEENABLED="True" `
    /INSTANCENAME="MSSQLSERVER" `
    /INSTANCEID="MSSQLSERVER" `
    /ENU="True" `
    /IACCEPTSQLSERVERLICENSETERMS="True" `
    /SQLCOLLATION="Latin1_General_CI_AS" `
    /FEATURES=SQLENGINE, IS `
    /SQLBACKUPDIR="D:\Backup\" `
    /SQLUSERDBDIR="D:\Data" `
    /SQLTEMPDBDIR="T:\TempDB\" `
    /SQLUSERDBLOGDIR="L:\LogFiles" `
    /INSTALLSHAREDDIR="B:\Program Files\Microsoft SQL Server" `
    /INSTALLSHAREDWOWDIR="B:\Program Files (x86)\Microsoft SQL Server" `
    /INSTANCEDIR="B:\Program Files\Microsoft SQL Server" `
    /SQLSVCACCOUNT="Dr-Foster\SQLSVC$SVCID$" `
    /SQLSYSADMINACCOUNTS="Dr-Foster\DBAs" "NT AUTHORITY\SYSTEM" `
    /SQLSVCSTARTUPTYPE="Automatic" `
    /AGTSVCACCOUNT="Dr-Foster\AGTSVC$SVCID$" `
    /AGTSVCSTARTUPTYPE="Automatic" `
    /SQLSVCINSTANTFILEINIT="True" `
    /ISTELSVCACCT="NT Service\SSISTELEMETRY150" `
    /ISTELSVCSTARTUPTYPE="Automatic" `
    /ISSVCSTARTUPTYPE="Automatic" `
    /ISSVCACCOUNT="Dr-Foster\ISSVC$SVCID$"  `
    /SQLMAXDOP="6"
###

# Add Server to DBA Server
if (!(Get-DbaRegServer -SqlInstance DBAPROD02 -ServerName $Computername -Group AllServers)) {
Add-DbaRegServer -SQLInstance DBAPROD02 -ServerName $computername -Group AllServers
    }
###


